const { expect } = require('chai');

const math = require('../math');
describe('a + b', () => {
    it('will calculate sum', () => {
        expect(math.aPlusB(1, 2)).to.eql(3);
    });
    it('will concatinate strings :)', async () => {
        expect(math.aPlusB('1', '2')).to.eql('12');
        expect(math.aPlusB('1', '3')).to.eql('13');
    });
});
